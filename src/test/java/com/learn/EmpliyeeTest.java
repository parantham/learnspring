package com.learn.test

package com.guru.webapp;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {


    @Test
    void testSortingEmployeeObject(){

        List<Employee> employeeList = new ArrayList<Employee>();

        List<Employee> sortedEmployeeList = new ArrayList<Employee>();


        employeeList.add( new Employee("Raj","Account",40));
        employeeList.add( new Employee("Raj","Finance",22));
        employeeList.add( new Employee("Raj","Account",55));
        employeeList.add( new Employee("Raj","Technology",27));
        employeeList.add( new Employee("Raj","Account",43));
        employeeList.add( new Employee("Raj","Technology",43));
        employeeList.add( new Employee("Raj","Finance",32));
        employeeList.add( new Employee("Raj","Account",19));


        sortedEmployeeList.add( new Employee("Raj","Account",19));
        sortedEmployeeList.add( new Employee("Raj","Account",40));
        sortedEmployeeList.add( new Employee("Raj","Account",43));
        sortedEmployeeList.add( new Employee("Raj","Account",55));
        sortedEmployeeList.add( new Employee("Raj","Finance",22));
        sortedEmployeeList.add( new Employee("Raj","Finance",32));
        sortedEmployeeList.add( new Employee("Raj","Technology",27));
        sortedEmployeeList.add( new Employee("Raj","Technology",43));


        //System.out.println("Collection before sort : \n" +employeeList);
        Collections.sort(employeeList);
        //System.out.println("Collection after sort : \n" +employeeList);

        assertEquals(sortedEmployeeList,employeeList);





    }
    @Test
    void compareEmployeeEquals(){

        assertEquals(new Employee("Raj","Account",14),
                new Employee("Raj","Account",14)
                );
    }

    @Test
    void compareEmployeeLessAge(){

        assertEquals(-1, new Employee("Raj","Account",14).compareTo(
                new Employee("Raj","Account",15)) );
    }

    @Test
    void compareEmployeeGreaterAge(){

        assertEquals(1, new Employee("Raj","Account",19).compareTo(
                new Employee("Raj","Account",15)) );
    }

    @Test
    void compareEmployeeGreaterDepartment(){

        assertEquals(5, new Employee("Raj","Finance",13).compareTo(
                new Employee("Raj","Account",15)) );
    }

    @Test
    void compareEmployeeLesserDepartment(){

        assertTrue( new Employee("Raj","Finance",40).compareTo(
                new Employee("Raj","Technology",15)) < 0 );
    }



    @Test
    void getName() {
    }

    @Test
    void testToString() {
    }

    @Test
    void setName() {
    }

    @Test
    void getDepartmentName() {
    }

    @Test
    void setDepartmentName() {
    }

    @Test
    void getAge() {
    }

    @Test
    void setAge() {
    }
}
