
import java.util.Objects;

public class Employee implements Comparable  {

    String name;
    String departmentName;
    Integer age;

    public Employee(String name, String departmentName, int age) {
        this.name = name;
        this.departmentName = departmentName;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", departmentName='" + departmentName + '\'' +
                ", age='" + age + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {

        return this.compareTo(o)==0 ? true :false;

    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDepartmentName(), getAge());
    }

    @Override
    public int compareTo(Object o) {
        Employee anotherEmployee = (Employee) o;
        int compareResult =1;
        if(this.getDepartmentName().compareTo(anotherEmployee.getDepartmentName()) ==0 ){

            if(this.getAge() == anotherEmployee.getAge() ){
                return 0;
            }else
                return this.getAge().compareTo(anotherEmployee.getAge());

        }else
            return this.getDepartmentName().compareTo(anotherEmployee.getDepartmentName()) ;
    }
}
